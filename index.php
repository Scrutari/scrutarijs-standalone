<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
$GLOBALS['scrutari'] = array();
$GLOBALS['scrutari']['engine'] = array();
require_once('../scrutarijs-conf.php');
$GLOBALS['scrutari']['loc'] = array();

initLang();
initTarget();
initPage();


switch($GLOBALS['scrutari']['page']) {
    case 'engine':
        initEngineName();
        initEngineOptions();
        initOrigin();
        initMode();
        initFramework();
        initHide();
        initQuery();
        initFilters();
        initDev();
        include('pages/engine.php');
        break;
    case 'frame':
        initEngineName();
        initSupplementaryParameters();
        initWidth();
        include('pages/frame.php');
        break;
    case 'navigation':
        include('pages/navigation.php');
        break;
    case 'blank':
        include('pages/blank.php');
        break;
    default :
        exit('Unknown page: '.$GLOBALS['scrutari']['page']);
}


function initEngineName() {
    $engineName = '';
    if (isset($_REQUEST['engine'])) {
        $engine = $_REQUEST["engine"];
        if (!array_key_exists($engine, $GLOBALS['scrutari']['conf']['engines'])) {
            exit("Unknown engine: ".$engine);
        }
        $engineName = $engine;
    } else {
        foreach($GLOBALS['scrutari']['conf']['engines'] as $key => $value) {
            $engineName = $key;
            break;
        }
    }
    if (strlen($engineName) > 0) {
        $GLOBALS['scrutari']['engine']['name'] = $engineName;
    } else {
        exit("Engine is undefined");
    }
}

function initWidth() {
    $width = "780px";
    if (isset($_REQUEST['width'])) {
        $widthParam = $_REQUEST['width'];
        $val = intval($widthParam);
        if ($val > 0) {
            $width = $val."px";
        } else if ($widthParam ==  'lg') {
            $width = "1000px";
        }
    }
    $GLOBALS['scrutari']['width'] = $width;
}

function initMode() {
    $mode = '';
    if (isset($_REQUEST['mode'])) {
        $mode = $_REQUEST['mode'];
    } else {
        $mode = '';
    }
    $GLOBALS['scrutari']['mode'] = $mode;
}

function initFramework() {
    $framework = 'bootstrap3';
    if (isset($_REQUEST['framework'])) {
        switch($_REQUEST['framework']) {
            case 'none':
            case 'bootstrap3':
            case 'bootstrap2':
                $framework = $_REQUEST['framework'];
        }
    }
    $GLOBALS['scrutari']['framework'] = $framework;
}

function initSupplementaryParameters() {
    $params = '';
    foreach($_REQUEST as $key => $value) {
        if ($key != 'width' && $key != 'page' && $key != 'engine' && $key != 'target' && $key != 'mode') {
            $params .= '&'.$key.'='.urlencode($value);
        }
    }
    $GLOBALS['scrutari']['params'] = $params;
}

function initEngineOptions() {
    $engineName = $GLOBALS['scrutari']['engine']['name'];
    $GLOBALS['scrutari']['engine']['name'] = $engineName;
    $engineArray = $GLOBALS['scrutari']['conf']['engines'][$engineName];
    foreach ($engineArray as $key => $value){
        switch($key) {
            case 'corpus':
                $GLOBALS['scrutari']['with-corpus'] = $value;
                break;
            default:
                $GLOBALS['scrutari']['engine'][$key] = $value;
        }
        
    }
    if (!array_key_exists("css-links", $engineArray)) {
        $GLOBALS['scrutari']['engine']['css-links'] = array();
    }
    if (!array_key_exists("js-links", $engineArray)) {
        $GLOBALS['scrutari']['engine']['js-links'] =  array();;
    }
}

function initOrigin() {
    $GLOBALS['scrutari']['origin'] = $GLOBALS['scrutari']['conf']['origin-prefix'].$GLOBALS['scrutari']['engine']['name'];
}

function initLang() {
    $lang = 'fr';
    $l10n = 'fr';
    initLoc('fr');
    $langParam = '';
    if (isset($_REQUEST['lang'])) {
        $langParam = $_REQUEST['lang'];
        
    } else if (isset($_REQUEST['langui'])) {
        $langParam = $_REQUEST['langui'];
    }
    if (preg_match('/^[-a-zA-Z_]+$/', $langParam)) {
        $lang = $langParam;
    }
    if ($lang != 'fr') {
        initLoc($lang);
        if (file_exists("static/scrutarijs/l10n/".$lang.".js")) {
            $l10n = $lang;
        }
    }
    $GLOBALS['scrutari']['lang'] = $lang;
    $GLOBALS['scrutari']['l10n'] = $l10n;
}

function initTarget() {
    $target = '_blank';
    if (isset($_REQUEST['target'])) {
        $target = $_REQUEST['target'];
    }
    $GLOBALS['scrutari']['target'] = $target;
}

function initPage() {
    $page = 'engine';
    if(isset($_REQUEST['page'])) {
        $page = $_REQUEST['page'];
    }
    $GLOBALS['scrutari']['page'] = $page;
}

function initQuery() {
    $query = '';
    $qid = '';
    if(isset($_REQUEST['q'])) {
        $q = trim($_REQUEST['q']);
        if (strlen($q) > 0) {
            $query = $q;
        }
    }
    if(isset($_REQUEST['qid'])) {
        $qid = trim($_REQUEST['qid']);
    }
    $GLOBALS['scrutari']['query'] = $query;
    $GLOBALS['scrutari']['qid'] = $qid;
}

function initFilters() {
    initFilter('baselist');
    initFilter('corpuslist');
    initFilter('categorylist');
    initFilter('langlist');
}

function initFilter($name) {
    if(isset($_REQUEST[$name])) {
        $GLOBALS['scrutari'][$name] = trim($_REQUEST[$name]);
    }
}

function initDev() {
    $dev = false;
    if(isset($_REQUEST['dev'])) {
        if ($_REQUEST['dev'] == "1") {
            $dev = true;
        }
    }
    $GLOBALS['scrutari']['dev'] = $dev;
}

function initHide() {
    $hide = '';
    if (isset($_REQUEST['hide'])) {
        $hide = $_REQUEST['hide'];
    } else {
        $hide = '';
    }
    $GLOBALS['scrutari']['hide'] = $hide;
}

function booleanToString($bool) {
    if ($bool) {
        return "true";
    } else {
        return "false";
    }
}

function initLoc($lang) {
    $filePath = 'l10n/'.$lang.'/loc.ini';
    if (file_exists($filePath)) {
        $locArray = parseLocArray(file($filePath));
        foreach($locArray as $key => $value) {
            $value = str_replace('"', '\"', $value);
            $value = json_decode('"'.$value.'"');
            $GLOBALS['scrutari']['loc'][$key] = $value;
        }
    }
}

function parseLocArray($lines) {
    $locArray = array();
    $count = count($lines);
    for($i = 0; $i < $count; $i++) {
        $line = trim($lines[$i]);
        if (strlen($line) == 0) {
            continue;
        }
        $firstChar = substr($line, 0, 1);
        switch($firstChar) {
            case ';':
            case '#':
            case '!':
            case '[':
                continue;
        }
        $idx = strpos($line, '=');
        if (!$idx) {
            continue;
        }
        $key = trim(substr($line, 0, $idx));
        $value = trim(substr($line, $idx + 1));
        $locArray[$key] = $value;
    }
    return $locArray;
}
