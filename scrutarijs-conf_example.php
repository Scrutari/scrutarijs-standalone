<?php
$GLOBALS['scrutari']['conf'] = array(
    'dev-path' => "dev-scrutarijs/svn/",
    'origin-prefix' => "coredemjs-",
    'engines' => array(
        "coredem" => array(
            "url" => "http://sct1.scrutari.net/sct/coredem/",
            "corpus" => false
            ),
        "irenees" => array(
            "url" => "http://sct1.scrutari.net/sct/irenees/",
            "corpus" => true,
            "corpus-sort" => "none"
            ),
        "socioeco" => array(
            "url" => "http://sct1.scrutari.net/sct/socioeco/",
            "corpus" => false
            ),
        "premiermai" => array(
            "url" => "http://sct1.scrutari.net/sct/premiermai/",
            "corpus" => true,
            "css-links" => array(
                "static/scrutarijs/scrutarijs.css",
                "http://static.autourdu1ermai.fr/new/css/main.css",
                "http://static.autourdu1ermai.fr/new/css/scrutari.css"
                ),
            "js-links" => array("http://static.autourdu1ermai.fr/new/js/scrutari.js")
            ),
        "ritimo" => array(
            "url" => "http://sct1.scrutari.net/sct/ritimo/",
            "corpus" => false
            ),
        "ritimo_catalogue" => array(
            "url" => "http://sct1.scrutari.net/sct/ritimo_catalogue/",
            "corpus" => false
            ),
        "flm" => array(
            "url" => "http://sct1.scrutari.net/sct/flm/",
            "corpus" => false
            ),
        "fph" => array(
            "url" => "http://sct1.scrutari.net/sct/fph_org/",
            "corpus" => false,
            "base-sort" => "none"
            ),
        "arga" => array(
            "url" => "http://sct1.scrutari.net/sct/arga_public/",
            "corpus" => true
            ),
        "eclm" => array(
            "url" => "http://sct1.scrutari.net/sct/eclm/",
            "corpus" => false,
            "fiche-fields" => "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon,sct:thumbnail"
            ),
        "dev" => array(
            "url" => "http://localhost:8080/scrutari/dev/",
            "corpus" => false
            )
    )
);