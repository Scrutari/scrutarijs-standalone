<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
$existingStructureMap = array();
$existingTemplatesMap = array();


function getPermalinkPattern() {
    // Get HTTP/HTTPS (the possible values for this vary from server to server)
    $permalink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']),array('off','no'))) ? 'https' : 'http';
    // Get domain portion
    $permalink .= '://'.$_SERVER['HTTP_HOST'];
    // Get path to script
    $permalink .= strtok($_SERVER['REQUEST_URI'],'?');
    // Add path info, if any
    if (!empty($_SERVER['PATH_INFO'])) $permalink .= $_SERVER['PATH_INFO'];

    $get = $_GET; // Create a copy of $_GET
    unset($get['q']);
    unset($get['qid']);
    unset($get['langui']);
    unset($get['lang']);
    unset($get['target']);
    $get['engine'] = $GLOBALS['scrutari']['engine']['name'];
    if (isset($get['mode'])) {
        $mode = $get['mode'];
        if ($mode == 'frame') {
            $get['page'] = 'frame';
        }
        unset($get['mode']);
    }
    $permalink .= '?'.http_build_query($get);
    $permalink .= '&qid=$QID&lang=$LANG';
    return $permalink;
}
 
function includeL10N($dirPath) {
    $files = scandir($dirPath);
    $count = count($files);
    $jsString = "var SCRUTARI_L10N = {\n";
    for($i = 0; $i < $count; $i++) {
        $fileName = $files[$i];
        if ($fileName == "loc.ini") {
            $locArray = parseLocArray(file($dirPath."/loc.ini"));
            foreach($locArray as $key => $value) {
                $jsString .= '"'.$key.'":"'.escapeQuote($value)."\",\n";
            }
            break;
        }
    }
    for($i = 0; $i < $count; $i++) {
        $fileName = $files[$i];
        if (strpos($fileName, ".html") > 0) {
            $html = file_get_contents($dirPath."/".$fileName);
            $html = preg_replace("/\s/", " ", $html);
            $jsString .= '"_ '.$fileName.'":"'.escapeQuote($html)."\",\n";
        }
    }
    $jsString .= "endfile: true\n};\n";
    return $jsString;
}
  
function escapeQuote($value) {
    $value = str_replace('"', '\"', $value);
    return $value;
}
 
 function includeHTML($dirPath, $defaultPath) {
    $jsString = "var SCRUTARI_HTML = {\n";
    $jsString .= "\tstructure:{\n";
    $jsString .= includeStructure($dirPath."structure/");
    $jsString .= includeStructure($defaultPath."structure/");
    $jsString .= "\n\t}";
    $jsString .= ",\n\ttemplates:{\n";
    $jsString .= includeTemplates($dirPath."templates/");
    $jsString .= includeTemplates($defaultPath."templates/");
    $jsString .= "\n\t}";
    $jsString .= "\n};\n";
    return $jsString;
 }
 
 function includeStructure($structurePath) {
    global $existingStructureMap;
    $jsString = "";
    $files = scandir($structurePath);
    $count = count($files);
    for($i = 0; $i < $count; $i++) {
         $fileName = $files[$i];
        if (strpos($fileName, ".html") > 0) {
            $key = substr($fileName, 0, strlen($fileName) - 5);
            if (!array_key_exists($key, $existingStructureMap)) {
                $jsString .= toProperty($key, $structurePath.$fileName, (count($existingStructureMap) > 0), "\t\t");
                $existingStructureMap[$key] = true;
            }
        }
    }
    return $jsString;
 }
 
 function includeTemplates($templatesPath) {
    global $existingTemplatesMap;
    $jsString = "";
    $files = scandir($templatesPath);
    $count = count($files);
    for($i = 0; $i < $count; $i++) {
        $fileName = $files[$i];
        if (strpos($fileName, ".html") > 0) {
            $key = substr($fileName, 0, strlen($fileName) - 5);
            if (!array_key_exists($key, $existingTemplatesMap)) {
                $jsString .= toProperty($key, $templatesPath.$fileName, (count($existingTemplatesMap) > 0), "\t\t");
                $existingTemplatesMap[$key] = true;
            }
        }
    }
    return $jsString;
 }
 
 function toProperty($key, $filePath, $next, $tabs) {
    $html = file_get_contents($filePath);
    $html = preg_replace("/\s+/", " ", $html);
    $result = "";
    if ($next) {
        $result .=  ",\n";
    }
    $result .= $tabs."\"".$key."\":\"".addslashes($html)."\"";
    return $result;
 }
 
 function addEngineOption($name, $jsName) {
     $result ="";
     if (array_key_exists($name, $GLOBALS['scrutari']['engine'])) {
         if (strlen($GLOBALS['scrutari']['engine'][$name]) > 0) {
            $result = "\n            ".$jsName.': "'.addslashes($GLOBALS['scrutari']['engine'][$name]).'",';
         }
         
     }
     echo $result;
 }
  function addOption($name, $jsName) {
     $result ="";
     if (array_key_exists($name, $GLOBALS['scrutari'])) {
         if (strlen($GLOBALS['scrutari'][$name]) > 0) {
            $result = "\n            ".$jsName.': "'.addslashes($GLOBALS['scrutari'][$name]).'",';
         }
         
     }
     echo $result;
 }
?>
<!DOCTYPE html>
<html lang="<?php echo $GLOBALS['scrutari']['lang'];?>">
<head>
<title>ScrutariJs</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="static/icon.png" type="image/png" rel="icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="static/jquery/3.4.1/jquery.min.js"></script>
<script src="static/jsrender/1.0.4/jsrender.min.js"></script>
<?php if ($GLOBALS['scrutari']['framework'] == 'bootstrap3') {?>
    <script src="static/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="static/bootstrap/3.3.7/css/bootstrap.min.css">
    
<?php } else if ($GLOBALS['scrutari']['framework'] == 'bootstrap2') {?>
    <script src="static/bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="static/bootstrap/2.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/bootstrap/2.3.2/css/bootstrap-responsive.min.css">
<?php } ?>
<?php
$scrutarijsCssPath = "";
$frameworkCssPath = "";
$themeCssPath = "";
if ($GLOBALS['scrutari']['dev']) {
    $devPath = $GLOBALS['scrutari']['conf']['dev-path'];
    $list = file($devPath.'list.txt');
    $count = count($list);
    for($i = 0; $i < $count; $i++) {
        $jsfile = trim($list[$i]);
        if ((strlen($jsfile) > 0) && (strpos($jsfile, '#') !== 0)) {
            echo '<script src="'.$devPath.'src/js/'.$jsfile.'"></script>'."\n";
        }
    }
    echo '<script src="'.$devPath.'src/frameworks/'.$GLOBALS['scrutari']['framework'].'/init.js"></script>'."\n";
    $scrutarijsCssPath = $devPath.'src/css/scrutarijs.css';
    $frameworkCssPath = $devPath.'src/frameworks/'.$GLOBALS['scrutari']['framework'].'/framework.css';
    $themeCssPath = $devPath.'src/frameworks/'.$GLOBALS['scrutari']['framework'].'/resources/theme.css';
} else {
    echo '<script src="static/scrutarijs/scrutarijs.js"></script>';
    echo "\n".'<script src="static/scrutarijs/l10n/'.$GLOBALS['scrutari']['l10n'].'.js"></script>';
    echo "\n".'<script src="static/scrutarijs/frameworks/'.$GLOBALS['scrutari']['framework'].'.js"></script>';
    $scrutarijsCssPath = 'static/scrutarijs/scrutarijs.css';
    $frameworkCssPath = 'static/scrutarijs/frameworks/'.$GLOBALS['scrutari']['framework'].'.css';
    $themeCssPath = 'static/scrutarijs/frameworks/'.$GLOBALS['scrutari']['framework'].'/theme.css';
}
echo '<link rel="stylesheet" href="'.$scrutarijsCssPath.'">'."\n";
if (file_exists($frameworkCssPath)) {
    echo '<link rel="stylesheet" href="'.$frameworkCssPath.'">'."\n";
}
if (file_exists($themeCssPath)) {
    echo '<link rel="stylesheet" href="'.$themeCssPath.'">'."\n";
}
$count = count($GLOBALS['scrutari']['engine']['js-links']);
for($i = 0; $i < $count; $i++) {
    echo '<script src="'.$GLOBALS['scrutari']['engine']['js-links'][$i].'"></script>'."\n";
}
$count = count($GLOBALS['scrutari']['engine']['css-links']);
for($i = 0; $i < $count; $i++) {
    echo '<link rel="stylesheet" href="'.$GLOBALS['scrutari']['engine']['css-links'][$i].'">'."\n";
}
?>
<script>
    <?php if ($GLOBALS['scrutari']['dev']) {
        echo includeL10N($GLOBALS['scrutari']['conf']['dev-path']."src/l10n/".$GLOBALS['scrutari']['l10n']);
        echo includeHTML($GLOBALS['scrutari']['conf']['dev-path']."src/frameworks/".$GLOBALS['scrutari']['framework']."/", $GLOBALS['scrutari']['conf']['dev-path']."src/frameworks/_default/");
    }?>
    var scrutari_client = null;
    function setCurrentFicheBlock(code, scroll) {
        if (scrutari_client.currentFicheCode) {
            scrutari_client.$block("fiche_" + scrutari_client.currentFicheCode).removeClass("scrutaristandalone-CurrentFicheBlock");
        }
        scrutari_client.currentFicheCode = code;
        var $ficheBlock = scrutari_client.$block("fiche_" + code);
        $ficheBlock.addClass("scrutaristandalone-CurrentFicheBlock");
        if (scroll) {
            var top = $ficheBlock.offset().top;
            var $window =  $(window);
            var windowTop = $window.scrollTop();
            var windowBottom = windowTop + $window.height();
            if ((top < windowTop) || (top > (windowBottom - 20))) {
                if ($window.height() > 300) {
                    top = top - 150;
                }
                $window.scrollTop(top);
            }
        }
    }
    var framePaginationChangeHook = function (paginationFicheArray, paginationChangeArguments) {
        var client = this;
        var _linkClick = function () {
            var linkName = $(this).data('scrutariLink');
            var idx = linkName.indexOf('_');
            var code = linkName.substring(idx + 1);
            setCurrentFicheBlock(code, false);
            window.parent.frames["Navigation"].setCurrentFiche(code, paginationFicheArray);
        };
        window.parent.frames["Navigation"].clear();
        if (paginationChangeArguments.categoryName) {
            $("[data-scrutari-block='fiches_" + paginationChangeArguments.categoryName + "'] a.scrutari-fiche-Link").click(_linkClick);
        } else {
            $("a.scrutari-fiche-Link").click(_linkClick);
        }
    };
    var scrutari_templateFactory = null;
    var scrutari_callback = function (client) {
         scrutari_client = client;
    };
    $(function () {
        var scrutariConfig = new Scrutari.Config("<?php echo $GLOBALS['scrutari']['engine']['name'];?>","<?php echo $GLOBALS['scrutari']['engine']['url'];?>", "<?php echo $GLOBALS['scrutari']['lang'];?>", "<?php echo $GLOBALS['scrutari']['origin'];?>", {<?php
            addEngineOption('fiche-fields', 'ficheFields');
        ?>"":""});
        Scrutari.Client.init(scrutariConfig, "scrutariClient", {<?php
    if (array_key_exists('with-corpus', $GLOBALS['scrutari'])) {
         echo "\n            ".'withCorpus'.': '.booleanToString($GLOBALS['scrutari']['with-corpus']).',';
     }
    addEngineOption('base-sort', 'baseSort');
    addEngineOption('corpus-sort', 'corpusSort');
    addEngineOption('category-sort', 'categorySort');
    addEngineOption('lang-sort', 'langSort');
    addOption('target', 'ficheTarget');
    addOption('hide', 'ignoreList');
    addOption('query', 'initialQuery');
    addOption('qid', 'initialQId');
    if ($GLOBALS['scrutari']['mode'] == 'frame') {
        echo "\n            "."hooks: {paginationChange: framePaginationChangeHook}".',';
    }
?>

            permalinkPattern: "<?php echo addslashes(getPermalinkPattern());?>",
            initialFilters: {
<?php                addOption('baselist', 'baselist');
                addOption('corpuslist', 'corpuslist');
                addOption('categorylist', 'categorylist');
                addOption('langlist', 'langlist'); ?>
                end: true
            },
            templateFactory: scrutari_templateFactory          
        }, scrutari_callback);
    });
</script>
<style>
    .scrutaristandalone-CurrentFicheBlock {
        background-color: #e9e9e9;
    }
    
    .scrutaristandalone-Client {
        max-width: 1170px;
        margin: auto;
    }
</style>
<?php
$customFile = "custom/".$GLOBALS['scrutari']['engine']['name'].".php";
if (file_exists($customFile)) {
    include($customFile);
}
?>
</head>
<body>
<?php 
$scrutariClientClass = "scrutaristandalone-Client";

switch($GLOBALS['scrutari']['framework']) {
    case 'bootstrap3':
    case 'bootstrap2':
        $scrutariClientClass = "container";
}
?>
<div class="<?php echo $scrutariClientClass; ?>" id="scrutariClient">
</div>
</body>
</html>
