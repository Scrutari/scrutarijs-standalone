<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ScrutariJs</title>
    <link href="images/icon.png" type="image/png" rel="icon">
</head>
<frameset cols="<?php echo $GLOBALS['scrutari']['width']; ?>,*" >
    <frame name="Engine" src="?page=engine&amp;mode=frame&amp;width=<?php echo $GLOBALS['scrutari']['width']; ?>&amp;target=Fiche&amp;engine=<?php echo $GLOBALS['scrutari']['engine']['name'];echo htmlentities($GLOBALS['scrutari']['params']); ?>">
    <frameset rows="40px,*">
        <frame name="Navigation" src="?page=navigation&amp;langui=<?php echo $GLOBALS['scrutari']['lang'];?>">
        <frame name="Fiche" src="?page=blank&amp;langui=<?php echo $GLOBALS['scrutari']['lang'];?>">
    </frameset>
</frameset>
</html>

