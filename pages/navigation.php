<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
 
function loc($key) {
    if (array_key_exists($key, $GLOBALS['scrutari']['loc'])) {
        echo $GLOBALS['scrutari']['loc'][$key];
    } else {
        echo $key;
    }
}
 
?>
<!DOCTYPE html>
<html lang="<?php echo $GLOBALS['scrutari']['lang'];?>">
<head>
<title>ScrutariJs</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="static/jquery/3.4.1/jquery.min.js"></script>
<script src="static/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="static/bootstrap/3.3.7/css/bootstrap.min.css"><script>
var globalFicheArray;
var globalCurrentIndex = -1;
var colon = "<?php loc('_ colon')?>";

function setCurrentFiche (code, plageFicheArray) {
    globalFicheArray = plageFicheArray;
    var length = globalFicheArray.length;
    for(var i = 0; i < length; i++) {
        var fiche = globalFicheArray[i];
        if (fiche.codefiche == code) {
            globalCurrentIndex = i;
            break;
        }
    }
    checkButtons();
}

function checkButtons () {
    var previousButton = $("#previousButton");
    if (globalCurrentIndex > 0) {
        previousButton.prop("disabled", false);
        setButtonTitle(previousButton, globalFicheArray[globalCurrentIndex - 1]);
    } else {
        previousButton.prop("disabled", true);
        previousButton.attr("title", "");
    }
    var nextButton = $("#nextButton");
    if ((globalFicheArray) && (globalCurrentIndex < (globalFicheArray.length - 1))) {
        nextButton.prop("disabled", false);
        setButtonTitle(nextButton, globalFicheArray[globalCurrentIndex + 1]);
    } else {
        nextButton.prop("disabled", true);
        nextButton.attr("title", "");
    }
    var currentButton = $("#currentButton");
    if (globalCurrentIndex != -1) {
        currentButton.prop("disabled", false);
        setButtonTitle(currentButton, globalFicheArray[globalCurrentIndex]);
    } else {
        currentButton.prop("disabled", true);
        currentButton.attr("title", "");
    }
}

function setButtonTitle (jqEl, fiche) {
    var titre = jqEl.find("span.sr-only").text();
    titre += colon;
    titre += " ";
    var length = fiche.mtitre.length;
    for (var i = 0; i < length; i++) {
        var obj = fiche.mtitre[i];
        if (typeof obj === 'string') {
            titre += obj;
        } else {
            titre += obj.s;
        }
    }
    jqEl.attr("title", titre);
};

function clear () {
    $("#previousButton").prop("disabled", true);
    $("#nextButton").prop("disabled", true);
    $("#currentButton").prop("disabled", true);
    globalFicheArray = null;
    globalCurrentIndex = -1;
}

$(function () {
    $("#previousButton").click(function () {
        if (globalCurrentIndex > 0) {
            globalCurrentIndex--;
            var fiche = globalFicheArray[globalCurrentIndex];
             window.parent.frames["Engine"].setCurrentFicheBlock(fiche.codefiche, true);
            window.parent.frames["Fiche"].location = fiche.href;
        }
        checkButtons();
        return false;
    });
    $("#nextButton").click(function () {
        if (globalCurrentIndex < (globalFicheArray.length -1)) {
            globalCurrentIndex++;
            var fiche = globalFicheArray[globalCurrentIndex];
            window.parent.frames["Engine"].setCurrentFicheBlock(fiche.codefiche, true);
            window.parent.frames["Fiche"].location = fiche.href;
        }
        checkButtons();
        return false;
    });
    $("#currentButton").click(function () {
        var fiche = globalFicheArray[globalCurrentIndex];
         window.parent.frames["Engine"].setCurrentFicheBlock(fiche.codefiche, true);
        window.parent.frames["Fiche"].location = fiche.href;
    });
});
</script>
</head>
<body>
<div class="text-center">
<button type="button" class="btn btn-default" id="previousButton" disabled>
  <span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only"><?php loc('_ button_previous')?></span>
</button>
<button type="button" class="btn btn-default" id="currentButton" disabled>
  <span class="glyphicon glyphicon-chevron-down"></span><span class="sr-only"><?php loc('_ button_current')?></span>
</button>
<button type="button" class="btn btn-default" id="nextButton" disabled>
<span class="sr-only"><?php loc('_ button_next')?></span><span class="glyphicon glyphicon-chevron-right"></span>
</button>
</div>
</body>
</html>
