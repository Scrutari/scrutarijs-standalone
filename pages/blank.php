<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
 
function loc($key) {
    if (array_key_exists($key, $GLOBALS['scrutari']['loc'])) {
        echo $GLOBALS['scrutari']['loc'][$key];
    } else {
        echo $key;
    }
}
 
?>
<!DOCTYPE html>
<html>
<head>
<title>ScrutariJs</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<p><em><?php echo loc("_ blank_message");?></em></p>
</body>
</html>